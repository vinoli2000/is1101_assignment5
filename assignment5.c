#include<stdio.h>

#define performanceCost 500
#define perAttendee 3


int noOfAttendees(double);
double income(double);
double cost(double);
double profit(double);


int	noOfAttendees(double price)
{
	return 120-(((price-15)/5)*20);
}

double income(double price)
{
	return noOfAttendees(price)*price;
}

double cost(double price)
{
	return noOfAttendees(price)*perAttendee+performanceCost;
}

double profit(double price)
{
	return income(price)-cost(price);
}

int main()
    {
	double price;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(price=5;price<50;price+=5)
	{
		printf("Ticket Price = Rs.%2f\t\t Profit = Rs.%2f\n",price,profit(price));
		printf("------------------------------------------------------------------\n\n");
    }
		return 0;
	}
